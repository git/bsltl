%  Copyright (C) 2015, 2016   Fernando Pujaico Rivera
%
%  This file is a part of the Bio Speckle Laser Tool Library (BSLTL) package.
%
%  This BSLTL computer package is free software; you can redistribute it
%  and/or modify it under the terms of the GNU General Public License as
%  published by the Free Software Foundation; either version 3 of the
%  License, or (at your option) any later version.
%
%  This BSLTL computer package is distributed hoping that it could be
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, please download it from
%  <http://www.gnu.org/licenses>.

function hf=imagesc_with_points(MAT, POINTS,varargin)

%
%  This function is similar to imagesc, with the difference that this also draw 
%  the points contained in the variable POINTS. Additionally can be selected the
%  color of the points.
%
%  imagesc_with_points(MAT, POINTS);
%  hf=imagesc_with_points(MAT, POINTS);
%  hf=imagesc_with_points(MAT, POINTS,'w');
%
%  
%  Input:
%  MAT       is a matrix that will be used like the canvas to draw the points. 
%  POINTS    is a matrix with two columns and M lines. Thus, each line represent 
%            one point in study.(line,column).
%  OPTION    is the color of the points, by default OPTION='r', you can try 'r',
%            'g', 'b', 'w','k'.
%
%  Output:
%  hf        returns the figure handler.
%
%
%  For help, bug reports and feature suggestions, please visit:
%  http://www.nongnu.org/bsltl
%

%  Code developed by:  Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>               
%  Code documented by: Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>
%  Code reviewed by:   
%  
%  Date: 20 of February of 2017.
%  Review: 28 of March of 2017.
%

	if (nargin>2)
		if(~ischar(varargin{1}))
			error('The third parameter should be a string.');
		else
			OPTIONS=varargin{1};
		end
	else
		OPTIONS='r';
	end

	hf=0;

	SIZE=size(MAT);
	M = size(POINTS,1);
	L = size(POINTS,2);

	if (L==2)
		lin=POINTS(:,1);
		col=POINTS(:,2);
	else
		[lin, col] = ind2sub (SIZE, POINTS(:,1));
	end

	hf=imagesc(MAT);
	hold on;
	scatter(col,lin,OPTIONS);
	hold off;
end
