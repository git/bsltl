%  Copyright (C) 2018, 2019   Fernando Pujaico Rivera
%
%  This file is a part of the Bio Speckle Laser Tool Library (BSLTL) package.
%
%  This BSLTL computer package is free software; you can redistribute it
%  and/or modify it under the terms of the GNU General Public License as
%  published by the Free Software Foundation; either version 3 of the
%  License, or (at your option) any later version.
%
%  This BSLTL computer package is distributed hoping that it could be
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, please download it from
%  <http://www.gnu.org/licenses>.

function [Ymean Ystd POINTS]=get_lux_level(DATA,M,varargin)
%
%  This function creates an analysis in time of illumination level in some points 
%  in the datpack. 
%  A set of M points (pixels) randomly are selected in the datapack, 
%  and through DATA(:,:,k) for all k value. These points can be selected with
%  uniform distribution or a gaussian distribution.
%
%  [YMEAN YSTD POINTS]=get_lux_level(DATA,M);
%  [YMEAN YSTD POINTS]=get_lux_level(DATA,M,'on');
%  [YMEAN YSTD POINTS]=get_lux_level(DATA,M,Sigma);
%  [YMEAN YSTD POINTS]=get_lux_level(DATA,M,Sigma,'on');
%  
%  Input:
%  DATA    is the speckle data pack. Where DATA is a 3D matrix created grouping NTIMES 
%          intensity matrices with NLIN lines and NCOL columns. When N=size(DATA), then
%          N(1,1) represents NLIN and
%          N(1,2) represents NCOL and
%          N(1,3) represents NTIMES.
%  M       is the number of points in analysis.
%  Sigma   [Optional]is the standard deviation in pixels. If this variable is used
%          then the points are selected using a gaussian distribution. 
%  Show    [Optional] can be used in the last position of input, and its function 
%          is used to enable a graphic output of the selected points that formed the
%          THSP. Show='on', Show='on-red', Show='on-red-filled', Show='on-green', 
%          Show='on-green-filled', Show='on-blue' , Show='on-blue-filled'
%          Show='on-cyan', Show='on-cyan-filled', Show='on-magenta', 
%          Show='on-magenta-filled', Show='on-yellow', Show='on-yellow-filled',
%          Show='on-black', Show='on-black-filled', Show='on-gray' or  
%          Show='on-gray-filled', Show='on-white' or  
%          Show='on-white-filled' to enable. 
%          It is disabled in other cases, by default Show='off'.
%          Show='on' plot the points in the color red, in other cases are used the
%          specified colors.
%
%  Output:
%  YMEAN  is the mean illumination level in M points.
%  YSTD   is the standard deviation value (arround) of illumination level in M points.
%  POINTS [Optional] is a matrix with two columns and 
%         M lines. Thus, each line represents one point in study, (line,column).
%
%
%  For help, bug reports and feature suggestions, please visit:
%  http://nongnu.org/bsltl/
%

%  Code developed by:  Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>   
%  Code documented by: Fernando Pujaico Rivera <fernando.pujaico.rivera@gmail.com>
%  
%  Date: 09 of December of 2018.
%  Review: 
%
    ENABLEG='off';
    GAUSSIAN=false;
    Sigma=0;
    if nargin>=3
        for II=1:length(varargin)
            if(isnumeric(varargin{II}))
                GAUSSIAN=true;
                Sigma=varargin{II};
            elseif(ischar(varargin{II}))
                ENABLEG=varargin{II};
            end
        end
    endif
    
    if(GAUSSIAN==true)
        [Ymean Ystd POINTS]=get_gaussian_lux_level(DATA,M,Sigma,ENABLEG);
    else
        [Ymean Ystd POINTS]=get_random_lux_level(DATA,M,ENABLEG);
    end
 
    if strcmp(ENABLEG,'on')
        figure(gcf+1);
        errorbar([1:size(DATA,3)],Ymean,Ystd,'-.');
        xlim([0 size(DATA,3)]);
        ylim([0 1.1*(max(Ymean)+max(Ystd))]);
        xlabel('Image sample')
        ylabel('Mean illumination level')
    endif


endfunction



function [Ymean Ystd POINTS]=get_gaussian_lux_level(DATA,M,Sigma,ENABLEG)
    colormap(gray)

    GAVD=graphavd(DATA,'off');
    [THSP POINTS]= thsp_gaussian(DATA,M,Sigma,GAVD,ENABLEG);

    Ymean=mean(THSP);
    Ystd=std(THSP);
endfunction

function [Ymean Ystd POINTS]=get_random_lux_level(DATA,M,ENABLEG)
    colormap(gray)

    [THSP POINTS]= thsp_random(DATA,M,ENABLEG);

    Ymean=mean(THSP);
    Ystd=std(THSP);
endfunction
